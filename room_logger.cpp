//
// The file is created and updated by  Devendra Kumar for Risk latte AI Inc.
// Please do not copy without proper authorisation from Risk latte.
// Just created specifically to an instance server. 
//

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
using namespace std;

vector<string> split(string s, string delimiter){
	vector<string> collection;
	size_t pos = 0;
	string token;
	while ((pos = s.find(delimiter)) != string::npos) {
    		token = s.substr(0, pos);
    		collection.push_back(token);
    		s.erase(0, pos + delimiter.length());
		collection.push_back(s);
	}
	return collection;
}

int main() {	
string str;
string id;
string room;
string time;
vector<string> parsed;
vector<string> temp;
vector<string> temp3;
string temp2;
fstream my_file;
ofstream my_file2;

	my_file.open("/var/log/jitsi/jicofon.log", ios::in);
	if (!my_file) {
		cout << "No such file";
	}

	my_file2.open("/usr/share/jitsi-meet/userlogs.txt", ios::out);
	if (!my_file2) {
		cout << "No file created";
	}	

	else {
		my_file2<<"{LogData: ";
		while (1) {
			getline(my_file, str);
			if (my_file.eof())
				break;
			parsed = split( str, "Chat room event ChatRoomMemberPresenceChangeEvent");
			vector<string> refine_parsed;
			if(parsed.size()>1){
				time= parsed[0];
				my_file2<<"{ TimeStamp: "<< split(split(time,"INFO:")[0], "Jicofo ")[1]<<", ";
				refine_parsed = split(parsed[1], " ");
				my_file2<< "Type of event: " << split(refine_parsed[0], "=")[1] <<", ";
				temp = split(refine_parsed[1], "=");
				temp2 = split(temp[1], " member=ChatMember")[1];
				temp3 = split(temp2, "@");
				id= temp3[1];
				room =split(temp3[0],"[")[1];
				my_file2<< "Room: "<< room<<", ";
				my_file2<< "Member id: "<< split(room+"@"+id, ",")[0] <<" }, ";
			}
		}
		my_file2<< "}";

	}
	my_file.close();
	my_file2.close();
	return 0;
}
